﻿using System;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;
using Snowflake.Data.Client;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Utilities;

namespace SnowflakeStoredProcedure
{
    class Program
    {
        protected static string connectionString { get; set; }

        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            //TODO Exception handling
            dynamic activity = JsonConvert.DeserializeObject(File.ReadAllText("activity.json"));
            var extendedParams = activity.typeProperties.extendedProperties;

            Console.WriteLine("activity:" + Environment.NewLine + activity.ToString());

            //Build the connection string
            connectionString = Utilities.Common.BuildSnowflakeConnectionString(extendedParams);
            //Console.WriteLine($"Connection string: {connectionString}");

            string sprocSql = GetSqlToExecuteFromAzureBlob(extendedParams);

            using (IDbConnection conn = new SnowflakeDbConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                Console.WriteLine(
                    $"{DateTime.Now.ToUniversalTime()} " +
                    $": Connect to Snowflake succeeded"
                    );
                
                if (!String.IsNullOrEmpty(sprocSql))
                {
                    Console.WriteLine(
                        $"{DateTime.Now.ToUniversalTime()} " +
                        $": Next, will try the query passed in via extended parameter key 'snowflakeStoredProcedureName' and value: '" +
                        $"{extendedParams.snowflakeStoredProcedureName}.sql'" + 
                        Environment.NewLine + 
                        $"sql statement: { sprocSql }"
                        );

                    IDbCommand cmd = conn.CreateCommand();
                    cmd.CommandText = sprocSql;
                    IDataReader reader = cmd.ExecuteReader();
                    Console.WriteLine("Snowflake output: ");
                    while (reader.Read())
                    {
                        Console.WriteLine(reader.GetString(0));
                    }
                }


                conn.Close();
            }

            // Stop timing.
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("Completed in : {0}", stopwatch.Elapsed);


        }

        private static string GetSqlToExecuteFromAzureBlob(dynamic extendedParams)
        {
            string sprocSql = "";
            try
            {
                sprocSql = Utilities.Common.ReadFromBlob((string)extendedParams.azureBlobContainerName,
                    (string)extendedParams.snowflakeDatabaseName,
                    (string)extendedParams.snowflakeSchemaName,
                    (string)extendedParams.snowflakeStoredProcedureName);
                Console.WriteLine($"GetSqlToExecuteFromAzureBlob: {sprocSql}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error retrieving Stored Procedure from Azure Container {ex.Message}");
            }

            return sprocSql;
        }
    }
}
