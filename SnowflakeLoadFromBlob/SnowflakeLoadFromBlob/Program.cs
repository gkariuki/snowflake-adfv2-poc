﻿using Snowflake.Data.Client;
using System;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Diagnostics;
using System.Linq;
using Utilities;

namespace BlobToSnowflakeDataLoader
{
    class Program
    {
        protected static string connectionString { get; set; }

        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            dynamic activity = JsonConvert.DeserializeObject(File.ReadAllText("activity.json"));
            var extendedParams = activity.typeProperties.extendedProperties;         

            Console.WriteLine("activity:" + Environment.NewLine + activity.ToString());  

            // From LinkedServices
            dynamic linkedServices = JsonConvert.DeserializeObject(File.ReadAllText("linkedServices.json"));
            Console.WriteLine("linkedServices:" + Environment.NewLine + linkedServices.ToString());

            dynamic datasets = JsonConvert.DeserializeObject(File.ReadAllText("datasets.json"));
            Console.WriteLine("datasets:" + Environment.NewLine + datasets.ToString());

            //Build the snowflake connection string
            connectionString = Utilities.Common.BuildSnowflakeConnectionString(extendedParams);

            var sqlToLoadDataFromBlobSource = GenerateSqlToLoadFromBlobSource(extendedParams);
            
            using (IDbConnection conn = new SnowflakeDbConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                Console.WriteLine(
                    $"{DateTime.Now.ToUniversalTime()} " +
                    $": Connect to Snowflake succeeded"
                    );

                IDbCommand cmd = conn.CreateCommand();

                Console.WriteLine(
                    $"{DateTime.Now.ToUniversalTime()} " +
                    $": Next, try the query passed in via extended parameter key 'snowflakeSqlText':" +
                    $"{ activity.typeProperties.extendedProperties.snowflakeSqlText}"
                    );

                //cmd.CommandText = "select * from testgetguid";
                cmd.CommandText = activity.typeProperties.extendedProperties.snowflakeSqlText;
                IDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine(reader.GetString(0));
                }

                int count = LoadFromBlobSourceToSnowFlake(sqlToLoadDataFromBlobSource, cmd);                

                conn.Close();
            }

            // Stop timing.
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("Completed in : {0}", stopwatch.Elapsed);

            
        }
        private static string GenerateSqlToLoadFromBlobSource(dynamic extendedParams)
        {
            //copy into GK_SCHEMA.emp_basic from @gk_azure_stage/employees01.csv;
            var targetTable = extendedParams.snowflakeTargetTableName;
            var azureStage = extendedParams.snowflakeAzureStageName;

            string sourceFolder = extendedParams.sourceFolderPath.ToString();
            string snowflakeAzureStagePath = extendedParams.snowflakeAzureStagePath.ToString();
            if (sourceFolder.Trim().StartsWith(snowflakeAzureStagePath))
            {
                int length = snowflakeAzureStagePath.Length;
                if (length > 0)
                {
                    sourceFolder = sourceFolder.Substring(length);
                }
            }
            var sourceFile = extendedParams.sourceFileName;
            var fullSourcePath = String.IsNullOrEmpty(sourceFolder)
                ? sourceFile
                : sourceFolder + "/" + sourceFile;

            var loadSingleSql = String.Format(
                "copy into {0} from @{1}/{2} force=true;",
                targetTable, azureStage, fullSourcePath);

            Console.WriteLine(DateTime.Now.ToUniversalTime() + ": loadSingleSql:" +
                Environment.NewLine + loadSingleSql);

            return loadSingleSql;
        }

        private static int LoadFromBlobSourceToSnowFlake(dynamic sqlToLoadDataFromBlobSource, IDbCommand cmd)
        {
            int count;
            cmd.CommandText = sqlToLoadDataFromBlobSource;
            count = cmd.ExecuteNonQuery();
            Console.WriteLine(String.Format("Inserted {0} row(s)", count));
            return count;
        }
    }
}
