﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Helper methods in common with the different projects in the solution
 */
namespace Utilities
{
    public class Common
    {
        //Set-up the format of the Snowflake connection string
        private const string connectionStringFmt = "scheme=https;host={0}.{7}.snowflakecomputing.com;port=443;" +
            "user={1};password={2};account={0};warehouse={3};db={4};role={5};schema={6};region={7}";

        static void Main(string[] args)
        {
        }

        /*
         * A method used to build the snowflake connection string
         * @extendedParams - the parameters that were captured from the calling ADF pipeline
         * @return - the formatted Snowflake connection string
         */
        public static string BuildSnowflakeConnectionString(dynamic extendedParams)
        {
            //Read the connection parameters stored in the JSON file
            //TODO : Replace this with a key vault solution to avoid storing usernames and passwords in plain text
            dynamic connectionParams = JsonConvert.DeserializeObject(File.ReadAllText("parameters.json"));

            //Parse out the username and the password from the connection parameter file
            var usr = connectionParams.LULU_DEV.SNOWFLAKE_USER;
            var pwd = connectionParams.LULU_DEV.SNOWFLAKE_PASSWORD;

            //Format the connection string with the inputs from the parameters.json and activity.json files
            var s = String.Format(connectionStringFmt,
                            extendedParams.snowflakeAccountName,
                            usr,
                            pwd,
                            extendedParams.snowflakeWarehouseName,
                            extendedParams.snowflakeDatabaseName,
                            extendedParams.snowflakeRoleName,
                            extendedParams.snowflakeSchemaName,
                            extendedParams.snowflakeRegion
                            );
            return s;
        }

        /*
         * Utility to read a file from the blob store
         * @containerName - The container to read from
         * @databaseName - The database that the stored procedure references (folder hierarchy)
         * @schemaName - The schema that the stored procedure references (folder hierarchy)
         * @storedProcedureName - The stored procedure being referenced (file name)
         * @return - the text from the file in the blob
         */
        public static string ReadFromBlob(
            string containerName, string databaseName,
            string schemaName, string storedProcedureName)
        {
            //The connection string to the Blob account
            var storageAccountConnectionString = "DefaultEndpointsProtocol=https;AccountName=gksnowflake;AccountKey=NrmssTugkzFdOGVz9UF5FNmb4AkO+JmE+ZbqHLMr0JEt8pMvZ2zx/pdt7y3dD4rqnogra8SDfLANFoOs2N1urA==;EndpointSuffix=core.windows.net";

            //Generate a string containing the blob file path
            var blobPath = GenerateBlobFilePath(databaseName, schemaName, storedProcedureName);
            //Console.WriteLine("Getting content from blob file: " + blobPath);

            //Connect to the storage account, create a blob client, and download the text from the blob
            var storageAccount = CloudStorageAccount.Parse(storageAccountConnectionString);
            var myClient = storageAccount.CreateCloudBlobClient();
            var container = myClient.GetContainerReference(containerName);
            var blockBlob = container.GetBlockBlobReference(blobPath);
            var blobText = blockBlob.DownloadText();
            //Console.WriteLine(blobText);
            return blobText;
        }

        //A method to format the blob file path for stored procedures
        private static String GenerateBlobFilePath(String databaseName, String schemaName, String storedProcedureName)
        {
            return String.Format("{0}/{1}/Stored Procedures/{2}.sql", databaseName, schemaName, storedProcedureName);
        }
    }
}
